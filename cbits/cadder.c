#include "cadder.h"
#include <stdlib.h>

struct adder {
  int sum;
};


extern adder* adder_init() {
  adder* x = calloc(1,sizeof(struct adder));
  return x;
}

extern void adder_add(adder* x , int i) {
  x->sum += i;
}

extern int adder_read(adder* x) {
  return x->sum;
}

extern void adder_free(adder* x) {
  free(x);
}

