#ifndef CADDER_H
#define CADDER_H

typedef struct adder adder;

extern adder* adder_init();
extern void adder_add(adder* x , int i);
extern int adder_read(adder* x);
extern void adder_free(adder* x);

#endif

