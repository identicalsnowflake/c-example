module Adder
  ( newAdder
  , add
  , inspect
  ) where

import Control.Monad.Primitive
import Foreign.C.Types
import Foreign.ForeignPtr
import Foreign.Ptr


newtype Adder s = Adder (ForeignPtr ADDER_R)

data ADDER_R

foreign import ccall unsafe "adder.h adder_init" adder_init :: IO (Ptr ADDER_R)
foreign import ccall unsafe "adder.h adder_add" adder_add :: Ptr ADDER_R -> CInt -> IO ()
foreign import ccall unsafe "adder.h adder_read" adder_read :: Ptr ADDER_R -> IO CInt
foreign import ccall unsafe "adder.h &adder_free" adder_free :: FunPtr (Ptr ADDER_R -> IO ())


{-# INLINE newAdder #-}
newAdder :: PrimMonad m => m (Adder (PrimState m))
newAdder = unsafeIOToPrim do
  -- this incantation will let Haskell's garbage collector manage the pointer, so it will
  -- automatically call adder_free when the reference is no longer needed.
  Adder <$> (adder_init >>= newForeignPtr adder_free)

{-# INLINE withAdder #-}
withAdder :: PrimMonad m => Adder (PrimState m) -> (Ptr ADDER_R -> IO a) -> m a
withAdder (Adder d) f = unsafeIOToPrim $ withForeignPtr d f

{-# INLINE add #-}
add :: PrimMonad m => Adder (PrimState m) -> CInt -> m ()
add c = \a -> withAdder c \p -> do
  adder_add p a

{-# INLINE inspect #-}
inspect :: PrimMonad m => Adder (PrimState m) -> m CInt
inspect c = withAdder c \p -> do
  adder_read p

