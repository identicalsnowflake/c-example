module Main where

import Adder


main :: IO ()
main = do
  acc <- newAdder

  add acc 3
  add acc 5

  res <- inspect acc

  print res

